  var express = require('express');

  var bodyParser = require('body-parser');
  var mongoose = require('mongoose');

  var BMSmodels = require('../models/bmsmodel.js');

  var router = express.Router();

  router.use(bodyParser.json());

  /* GET battery page. */

  router.route('/')


    .get((req,res,next) => {
      BMSmodels.create(
        {
          count: req.query.count,
          str: req.query.str
        }
        ).then((bmsmodel)=> {
        console.log('bmsmodel created', bmsmodel);
        res.statusCode= 200;
        res.setHeader('Content-type','application/json');
        res.json({"status" : res.statusCode});    
      },(err) => next(err))
      .catch((err) => next(err));
    
    }); 


  module.exports = router;